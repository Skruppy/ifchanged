## This file is part of IfChangeD <https://gitlab.com/Skrupellos/ifchanged>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

from datetime import datetime
from typing import Tuple, Callable, Any, NoReturn
import dataclasses
import queue



class Eventloop:
	def __init__(self):
		self._timers = {}
		self._event_handlers = {}
		self._event_queue = queue.SimpleQueue()
	
	
	def start_and_wait(self):
		while True:
			if self._timers:
				next_timer = min(self._timers.values(), key=lambda x: x.time)
				timeout = max(0, next_timer.time - datetime.now().timestamp())
			else:
				timeout = None
			
			try:
				event, payload = self._event_queue.get(timeout=timeout)
				self._event_handlers[event](payload)
			
			except queue.Empty:
				next_timer.callback(*next_timer.args)
				del self._timers[next_timer.key]
	
	
	def add_named_timer(self, key, time, callback, args=()):
		self._timers[key] = _Timer(key, time, callback, args)
	
	def del_named_timer(self, key):
		if key in self._timers:
			del self._timers[key]
	
	def add_event_handler(self, event, callback):
		self._event_handlers[event] = callback
	
	def send_event(self, event, payload):
		self._event_queue.put((event, payload))


@dataclasses.dataclass
class _Timer:
	key: str
	time: float
	callback: Callable[..., NoReturn]
	args: Tuple[Any]
