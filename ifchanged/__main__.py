## This file is part of IfChangeD <https://gitlab.com/Skrupellos/ifchanged>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

from ifchanged import Cli

def main():
	cli = Cli()
	cli.run()

if __name__ == '__main__':
	main()
