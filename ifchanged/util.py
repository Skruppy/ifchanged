## This file is part of IfChangeD <https://gitlab.com/Skrupellos/ifchanged>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

class Dobject(dict):
	def __init__(self, *pargs, **kargs):
		d = {}
		for patch in pargs:
			d.update(patch)
		d.update(kargs)
		super().__init__(d)
	
	__getattr__ = dict.__getitem__
	__setattr__ = dict.__setitem__


def dobjectify(thing):
	if issubclass(type(thing), dict):
		return Dobject((k, dobjectify(v)) for k, v in thing.items())
	
	elif issubclass(type(thing), list):
		return list(map(dobjectify, thing))
	
	else:
		return thing


def ip_list(ips):
	return [ip.exploded for ip in ips]
