## This file is part of IfChangeD <https://gitlab.com/Skrupellos/ifchanged>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import importlib.util
import os
import sys



class Filter:
	def __init__(self, if_name):
		self.if_name = if_name
	
	@classmethod
	def pluginPath(cls):
		return 'filters'


class Action:
	def __init__(self, if_name):
		self.if_name = if_name
	
	@classmethod
	def pluginPath(cls):
		return 'actions'


search_pathes = [os.path.dirname(__file__)]


def loadAll(cls):
	alreadyLoaded = set()
	
	pluginPath = cls.pluginPath()
	pathes = map(lambda x: os.path.join(x, pluginPath), search_pathes) ## Build list of search pathes
	pathes = filter(os.path.isdir, pathes)                             ## Filter search pathes for actual directories
	for path in pathes:
		## Search a plugin directory for plugin files
		entries = os.scandir(path)
		entries = filter(lambda x: x.is_file() and not x.name[0] in ['.', '_'] and x.name.endswith('.py'), entries)
		
		## Load and yield not already loaded plugins
		for entrie in entries:
			if entrie.name in alreadyLoaded:
				continue
			alreadyLoaded.add(entrie.name)
			yield _findClass(cls, entrie.name[:-3], entrie.path)


def load(cls, name):
	if isinstance(name, list):
		return list(map(lambda x: load(cls, x), name))
	
	## Check name
	if os.path.dirname(name) != '':
		raise Exception(f'"{name}" is not a valid plugin name')
	
	## Find plugin path
	pluginPath = cls.pluginPath()
	for search_path in search_pathes:
		path = os.path.join(search_path, pluginPath, name+'.py')
		if os.path.isfile(path):
			break
		else:
			path = None
	
	if path is None:
		raise Exception(f'"{name}" is not a valid plugin from {search_pathes}')
	
	return _findClass(cls, name, path)


def _findClass(cls, name, path):
	## Evaluate plugin file
	spec = importlib.util.spec_from_file_location(name, path)
	module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(module)
	
	## Find (the one) plugin class
	plugin = list(filter(lambda x: type(x) is type and issubclass(x, cls) and x != cls, module.__dict__.values()))
	if len(plugin) > 1:
		raise Exception(f'More than one {cls.__module__}.{cls.__qualname__} plugin is defined in {path}')
	elif len(plugin) == 0:
		raise Exception(f'No {cls.__module__}.{cls.__qualname__} plugins are defined in {path}')
	
	plugin[0].pluginName = name
	plugin[0].pluginPath = path
	return plugin[0]
