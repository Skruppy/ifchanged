## This file is part of IfChangeD <https://gitlab.com/Skrupellos/ifchanged>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

import yaml

from ifchanged import eventloop
from ifchanged import monitor
from ifchanged import plugin
from ifchanged import util



class Cli:
	def __init__(self):
		self._filters = {x.pluginName: x for x in plugin.loadAll(plugin.Filter)}
		self._actions = {x.pluginName: x for x in plugin.loadAll(plugin.Action)}
		
		with open('ifmon.yaml') as f:
			config = util.dobjectify(yaml.safe_load(f))
		
		self._el = eventloop.Eventloop()
		if_mon = monitor.InterfaceMonitor(self._el)
		for cfg_if in config.get('interfaces', []):
			if_name = cfg_if.name
			timeout = cfg_if.get('timeout', 5)
			f = self._build_filter(cfg_if.get('filter'), if_name)
			
			cfg_actions = cfg_if.get('action')
			if cfg_actions is None:
				continue
			elif type(cfg_actions) != list:
				action_fn = self._build_action(cfg_actions, if_name)
			else:
				actions = [self._build_action(cfg, if_name) for cfg in cfg_actions]
				def action_fn(last_ips, staged_ips):
					for action in actions:
						action(last_ips, staged_ips)
			
			if_mon.watch_interface(if_name, f, action_fn, timeout)
		
		self._el.start_and_wait()
	
	
	def run(self):
		self._el.start_and_wait()
	
	
	def _build_filter(self, cfg_filter, if_name):
		if cfg_filter is None:
			return None
		
		if type(cfg_filter) == str:
			return self._filters[cfg_filter](if_name, {})
		
		if isinstance(cfg_filter, dict):
			return self._filters[cfg_filter.name](if_name, cfg_filter)
		
		assert()
	
	
	def _build_action(self, cfg_action, if_name):
		if type(cfg_action) == str:
			return self._actions[cfg_action](if_name, {})
		
		if isinstance(cfg_action, dict):
			return self._actions[cfg_action.name](if_name, cfg_action)
		
		assert()
