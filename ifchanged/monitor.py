## This file is part of IfChangeD <https://gitlab.com/Skrupellos/ifchanged>
## Copyright (c) Skruppy <skruppy@onmars.eu>
## SPDX-License-Identifier: Apache-2.0

from datetime import datetime
import ipaddress
import subprocess
import threading

from pyroute2 import IPRoute

from  ifchanged import eventloop
from  ifchanged import util



class InterfaceMonitor:
	def __init__(self, el):
		self._el = el
		self._ifs = {}
		self._named_ifs = {}
		self._el.add_event_handler('RTM_NEWLINK', self._handle_new_line)
		self._el.add_event_handler('RTM_DELLINK', self._handle_del_link)
		self._el.add_event_handler('RTM_NEWADDR', self._handle_new_addr)
		self._el.add_event_handler('RTM_DELADDR', self._handle_del_addr)
		reader_t = _Reader(el)
		reader_t.start()
	
	def watch_interface(self, name, ip_filter, callback, timeout):
		watchers = self._named_ifs.get(name)
		if watchers is None:
			self._named_ifs[name] = watchers = []
		watchers.append(_NamedInterface(name, ip_filter, callback, timeout))
	
	## Handle new interfaces RTNL message from _Reader class
	def _handle_new_line(self, msg):
		i = self._ifs.get(msg.index)
		if i:
			i.set_name(msg.attrs.IFLA_IFNAME)
		else:
			self._ifs[msg.index] = _Interface(self, msg.index, msg.attrs.IFLA_IFNAME)
	
	## Handle deleted interfaces RTNL message from _Reader class
	def _handle_del_link(self, msg):
		i = self._ifs.get(msg.index)
		if i:
			i.delete()
			del self._ifs[msg.index]
	
	## Handle new addresses RTNL message from _Reader class
	def _handle_new_addr(self, msg):
		self._ifs[msg.index].add_ip(self._parse_ip(msg))
	
	## Handle deleted addresses RTNL message from _Reader class
	def _handle_del_addr(self, msg):
		self._ifs[msg.index].del_ip(self._parse_ip(msg))
	
	## Callback for _Interface class
	def ip_update(self, name, ips):
		print(f'New IP set for {name}: {ips}')
		for named_if in self._named_ifs.get(name, []):
			if named_if.update_ips(ips):
				self._el.add_named_timer(
					named_if,
					datetime.now().timestamp() + named_if.settle_time,
					self._settled,
					(named_if, ),
				)
			else:
				self._el.del_named_timer(named_if)
	
	## Timer event scheduled by ip_update()
	def _settled(self, named_if):
		named_if.commit_ips()
	
	@classmethod
	def _parse_ip(cls, msg):
		if   msg.family == 2:  return ipaddress.IPv4Address(msg.attrs.IFA_LOCAL)
		elif msg.family == 10: return ipaddress.IPv6Address(msg.attrs.IFA_ADDRESS)
		else: assert()


class _Reader(threading.Thread):
	def __init__(self, ev):
		super().__init__(daemon=True)
		self._ev = ev
		
	def run(self):
		with IPRoute() as ipr:
			ipr.bind()
			
			for msg in map(self.parse_msg, ipr.get_links()):
				self._ev.send_event(msg.event, msg)
			
			for msg in map(self.parse_msg, ipr.get_addr()):
				self._ev.send_event(msg.event, msg)
			
			while True:
				for msg in map(self.parse_msg, ipr.get()):
					if msg.event in [
						'RTM_NEWLINK', 'RTM_DELLINK', 'RTM_NEWADDR', 'RTM_DELADDR',
					]:
						self._ev.send_event(msg.event, msg)
	
	@classmethod
	def parse_msg(cls, msg):
		msg = util.Dobject(msg)
		msg.attrs = util.Dobject(msg.attrs)
		return msg


class _Interface:
	def __init__(self, listener, idx, name):
		self._listener = listener
		self._idx = idx
		self._name = name
		self._ips = set()
		self._notify_changes()
	
	def set_name(self, name):
		if self._name == name: return
		self._notify_changes(set())
		self._name = name
		self._notify_changes()
	
	def add_ip(self, ip):
		if ip in self._ips: return
		self._ips.add(ip)
		self._notify_changes()
	
	def del_ip(self, ip):
		if ip not in self._ips: return
		self._ips.remove(ip)
		self._notify_changes()
	
	def delete(self):
		self._ips = None
		self._notify_changes(set())
	
	def _notify_changes(self, ips=None):
		self._listener.ip_update(self._name, self._ips if ips is None else ips)


class _NamedInterface:
	def __init__(self, name, ip_filter, callback, settle_time=5):
		self.name = name
		self._ip_filter = ip_filter
		self._callback = callback
		self.settle_time = settle_time
		self._last_ips = None
		self._staged_ips = None
	
	def update_ips(self, new_ips):
		self._staged_ips = new_ips if self._ip_filter is None else set(filter(self._ip_filter, new_ips))
		return self._last_ips is None or self._last_ips != self._staged_ips
	
	def commit_ips(self):
		self._callback(self._last_ips, self._staged_ips)
		self._last_ips = self._staged_ips
