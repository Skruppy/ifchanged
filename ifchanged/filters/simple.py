from ifchanged import plugin



class SimpleFilter(plugin.Filter):
	def __init__(self, if_name, config):
		super().__init__(if_name)
		self._version = config.get('version', 0)
		self._multicast = config.get('multicast')
		self._private = config.get('private')
		self._global = config.get('global')
		self._unspecified = config.get('unspecified')
		self._reserved = config.get('reserved')
		self._loopback = config.get('loopback')
		self._link_local = config.get('link_local')
		self._eui64_slaac = config.get('eui64_slaac')
	
	def __call__(self, ip):
		if self._version == 4 and ip.version != 4: return False
		elif self._version == 6 and ip.version != 6: return False
		if self._multicast is not None and ip.is_multicast != self._multicast: return False
		if self._private is not None and ip.is_private != self._private: return False
		if self._global is not None and ip.is_global != self._global: return False
		if self._unspecified is not None and ip.is_unspecified != self._unspecified: return False
		if self._reserved is not None and ip.is_reserved != self._reserved: return False
		if self._loopback is not None and ip.is_loopback != self._loopback: return False
		if self._link_local is not None and ip.is_link_local != self._link_local: return False
		if self._eui64_slaac is not None and ip.version == 6 and (ip.packed[11:13] == b'\xFF\xFE') == self._eui64_slaac: return False
		return True
