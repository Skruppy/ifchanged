import subprocess

from ifchanged import plugin



class OldExecAction(plugin.Action):
	def __init__(self, if_name, config):
		super().__init__(if_name)
		self._proc = None
	
	
	def __call__(self, old_ips, new_ips):
		if old_ips is None:
			old_ips = set()
		
		self.print('del', old_ips-new_ips)
		self.print('new', new_ips-old_ips)
	
	
	def print(self, action, ips):
		for ip in ips:
			proc = self.get_proc()
			proc.stdin.write(f'{self.if_name} {action} IPv{ip.version} {ip.exploded}\n'.encode('utf-8'))
			proc.stdin.flush()
	
	
	def get_proc(self):
		if self._proc is None:
			self._proc = subprocess.Popen('../testScript', shell=True, stdin=subprocess.PIPE)
		return self._proc
