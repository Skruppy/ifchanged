import subprocess
import enum
import json
import os

from ifchanged import plugin
from ifchanged import util



class JsonExecAction(plugin.Action):
	def __init__(self, if_name, config):
		super().__init__(if_name)
		self._cmd = config['cmd']
		self._method = Method[config.get('method', 'env')]
	
	
	def __call__(self, old_ips, new_ips):
		if old_ips is None:
			old_ips = set()
		
		data = {
			'old': util.ip_list(old_ips),
			'new': util.ip_list(new_ips),
			'deleted': util.ip_list(old_ips-new_ips),
			'added': util.ip_list(new_ips-old_ips),
		}
		
		data = json.dumps(data)
		stdin = data.encode('utf-8') if self._method == Method.stdin else None
		if self._method == Method.env:
			env = dict(os.environ)
			env['IFCHANGED_JSON'] = data
		else:
			env = None
		
		subprocess.run(self._cmd, shell=True, check=True, input=stdin, env=env)


class Method(enum.Enum):
	stdin = enum.auto()
	env = enum.auto()
