from ifchanged import plugin



class PrintAction(plugin.Action):
	def __init__(self, if_name, config):
		super().__init__(if_name)
	
	def __call__(self, old_ips, new_ips):
		if old_ips is None:
			print(f'YAYYAYAYA {self.if_name}: first add {new_ips}')
		else:
			print(f'YAYYAYAYA {self.if_name}: delete {old_ips-new_ips} -> add {new_ips-old_ips}')
