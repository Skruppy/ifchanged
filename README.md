## Kann ein interface was up ist umbeannnt werden?
## Löst ein ip l down RTM_DELADDR aus?
## $ ip link add dummy0 type dummy
## $ ip a add 10.4.0.0/24 dev dummy0
## $ ip a add fd3e:8cc1:cf35:400::1/64 dev dummy0
## $ ip l set dummy0 up
## $ ip link set dummy0 name dummy1
## $ ip l set dummy0 down
## $ ip a del 10.4.0.0/24 dev dummy0
## $ ip a del fd3e:8cc1:cf35:400::1/64 dev dummy0
## $ ip link del dummy0 type dummy

* Adding an IPv4 address will emit a single message
* Adding an IPv6 address will emit two events, with different flags
* Removing an IPv4 address will emit one event
* Removing an IPv6 address will emit only one event
* Renaming an interface will emit add events only for IPv4 addresses
* Setting an interface down will only remove all IPv6 addresses
* Setting an interface up will only emit add events for all IPv4 addresses

## IPv4: Use IFA_LOCAL (IFA_ADDRESS is different on PPPoE https://stackoverflow.com/a/4678747)
## IPv6: Has only IFA_ADDRESS

# {'attrs': [('IFA_ADDRESS', '10.1.0.10'),
#            ('IFA_LOCAL', '10.1.0.10'),
#            ('IFA_BROADCAST', '10.1.0.255'),
#            ('IFA_LABEL', 'wlp2s0'),
#            ('IFA_FLAGS', 512),
#            ('IFA_CACHEINFO', {'ifa_preferred': 86400, 'ifa_valid': 86400, 'cstamp': 427960208, 'tstamp': 427960217})],
#  'event': 'RTM_NEWADDR',
#  'family': 2,
#  'flags': 0,
#  'header': {'error': None,
#             'flags': 0,
#             'length': 88,
#             'pid': 1496553,
#             'sequence_number': 9590,
#             'stats': Stats(qsize=0, delta=0, delay=0),
#             'target': 'localhost',
#             'type': 20},
#  'index': 973,
#  'prefixlen': 24,
#  'scope': 0}
#
# {'attrs': [('IFA_ADDRESS', 'fe80::e8b:fdff:feca:cbf5'),
#            ('IFA_CACHEINFO', {'ifa_preferred': 4294967295, 'ifa_valid': 4294967295, 'cstamp': 427960203, 'tstamp': 427960203}),
#            ('IFA_FLAGS', 640)],
#  'event': 'RTM_NEWADDR',
#  'family': 10,
#  'flags': 128,
#  'header': {'error': None,
#             'flags': 0,
#             'length': 72,
#             'pid': 0,
#             'sequence_number': 0,
#             'stats': Stats(qsize=0, delta=0, delay=0),
#             'target': 'localhost',
#             'type': 20},
#  'index': 973,
#  'prefixlen': 64,
#  'scope': 253}
#
# {'__align': (),
#  'attrs': [('IFLA_IFNAME', 'dummy0'),
#            ('IFLA_TXQLEN', 1000),
#            ('IFLA_OPERSTATE', 'DOWN'),
#            ('IFLA_LINKMODE', 0),
#            ('IFLA_MTU', 1500),
#            ('UNKNOWN', {'header': {'length': 8, 'type': 50}}),
#            ('UNKNOWN', {'header': {'length': 8, 'type': 51}}),
#            ('IFLA_GROUP', 0),
#            ('IFLA_PROMISCUITY', 0),
#            ('IFLA_NUM_TX_QUEUES', 1),
#            ('IFLA_GSO_MAX_SEGS', 65535),
#            ('IFLA_GSO_MAX_SIZE', 65536),
#            ('IFLA_NUM_RX_QUEUES', 1),
#            ('IFLA_CARRIER', 1),
#            ('IFLA_QDISC', 'noop'),
#            ('IFLA_CARRIER_CHANGES', 0),
#            ('IFLA_PROTO_DOWN', 0),
#            ('IFLA_CARRIER_UP_COUNT', 0),
#            ('IFLA_CARRIER_DOWN_COUNT', 0),
#            ('IFLA_MAP', {'mem_start': 0, 'mem_end': 0, 'base_addr': 0, 'irq': 0, 'dma': 0, 'port': 0}),
#            ('IFLA_ADDRESS', '1a:9b:d9:99:96:69'),
#            ('IFLA_BROADCAST', 'ff:ff:ff:ff:ff:ff'),
#            ('IFLA_STATS64', {'rx_packets': 0, 'tx_packets': 0, 'rx_bytes': 0, 'tx_bytes': 0, 'rx_errors': 0, 'tx_errors': 0, 'rx_dropped': 0, 'tx_dropped': 0, 'multicast': 0, 'collisions': 0, 'rx_length_errors': 0, 'rx_over_errors': 0, 'rx_crc_errors': 0, 'rx_frame_errors': 0, 'rx_fifo_errors': 0, 'rx_missed_errors': 0, 'tx_aborted_errors': 0, 'tx_carrier_errors': 0, 'tx_fifo_errors': 0, 'tx_heartbeat_errors': 0, 'tx_window_errors': 0, 'rx_compressed': 0, 'tx_compressed': 0}),
#            ('IFLA_STATS', {'rx_packets': 0, 'tx_packets': 0, 'rx_bytes': 0, 'tx_bytes': 0, 'rx_errors': 0, 'tx_errors': 0, 'rx_dropped': 0, 'tx_dropped': 0, 'multicast': 0, 'collisions': 0, 'rx_length_errors': 0, 'rx_over_errors': 0, 'rx_crc_errors': 0, 'rx_frame_errors': 0, 'rx_fifo_errors': 0, 'rx_missed_errors': 0, 'tx_aborted_errors': 0, 'tx_carrier_errors': 0, 'tx_fifo_errors': 0, 'tx_heartbeat_errors': 0, 'tx_window_errors': 0, 'rx_compressed': 0, 'tx_compressed': 0}),
#            ('IFLA_XDP', '05:00:02:00:00:00:00:00'),
#            ('IFLA_LINKINFO', {'attrs': [('IFLA_INFO_KIND', 'dummy')]}),
#            ('IFLA_AF_SPEC', {'attrs': [('AF_INET', {'dummy': 65668, 'forwarding': 1, 'mc_forwarding': 0, 'proxy_arp': 0, 'accept_redirects': 1, 'secure_redirects': 1, 'send_redirects': 1, 'shared_media': 1, 'rp_filter': 2, 'accept_source_route': 0, 'bootp_relay': 0, 'log_martians': 0, 'tag': 0, 'arpfilter': 0, 'medium_id': 0, 'noxfrm': 0, 'nopolicy': 0, 'force_igmp_version': 0, 'arp_announce': 0, 'arp_ignore': 0, 'promote_secondaries': 1, 'arp_accept': 0, 'arp_notify': 0, 'accept_local': 0, 'src_vmark': 0, 'proxy_arp_pvlan': 0, 'route_localnet': 0, 'igmpv2_unsolicited_report_interval': 10000, 'igmpv3_unsolicited_report_interval': 1000}), ('AF_INET6', {'attrs': [('IFLA_INET6_FLAGS', 0), ('IFLA_INET6_CACHEINFO', {'max_reasm_len': 65535, 'tstamp': 454120861, 'reachable_time': 23845, 'retrans_time': 1000}), ('IFLA_INET6_CONF', {'forwarding': 0, 'hop_limit': 64, 'mtu': 1500, 'accept_ra': 1, 'accept_redirects': 1, 'autoconf': 1, 'dad_transmits': 1, 'router_solicitations': 4294967295, 'router_solicitation_interval': 4000, 'router_solicitation_delay': 1000, 'use_tempaddr': 0, 'temp_valid_lft': 604800, 'temp_preferred_lft': 86400, 'regen_max_retry': 3, 'max_desync_factor': 600, 'max_addresses': 16, 'force_mld_version': 0, 'accept_ra_defrtr': 1, 'accept_ra_pinfo': 1, 'accept_ra_rtr_pref': 0, 'router_probe_interval': 0, 'accept_ra_rt_info_max_plen': 0, 'proxy_ndp': 0, 'optimistic_dad': 0, 'accept_source_route': 0, 'mc_forwarding': 0, 'disable_ipv6': 0, 'accept_dad': 4294967295, 'force_tllao': 0, 'ndisc_notify': 0}), ('IFLA_INET6_STATS', {'num': 37, 'inpkts': 0, 'inoctets': 0, 'indelivers': 0, 'outforwdatagrams': 0, 'outpkts': 0, 'outoctets': 0, 'inhdrerrors': 0, 'intoobigerrors': 0, 'innoroutes': 0, 'inaddrerrors': 0, 'inunknownprotos': 0, 'intruncatedpkts': 0, 'indiscards': 0, 'outdiscards': 0, 'outnoroutes': 0, 'reasmtimeout': 0, 'reasmreqds': 0, 'reasmoks': 0, 'reasmfails': 0, 'fragoks': 0, 'fragfails': 0, 'fragcreates': 0, 'inmcastpkts': 0, 'outmcastpkts': 0, 'inbcastpkts': 0, 'outbcastpkts': 0, 'inmcastoctets': 0, 'outmcastoctets': 0, 'inbcastoctets': 0, 'outbcastoctets': 0, 'csumerrors': 0, 'noectpkts': 0, 'ect1pkts': 0, 'ect0pkts': 0, 'cepkts': 0}), ('IFLA_INET6_ICMP6STATS', {'num': 6, 'inmsgs': 0, 'inerrors': 0, 'outmsgs': 0, 'outerrors': 0, 'csumerrors': 0}), ('IFLA_INET6_TOKEN', '::'), ('IFLA_INET6_ADDR_GEN_MODE', 0)]})]})],
#  'change': 0,
#  'event': 'RTM_NEWLINK',
#  'family': 0,
#  'flags': 130,
#  'header': {'error': None,
#             'flags': 0,
#             'length': 1340,
#             'pid': 0,
#             'sequence_number': 0,
#             'stats': Stats(qsize=0, delta=0, delay=0),
#             'target': 'localhost',
#             'type': 16},
#  'ifi_type': 1,
#  'index': 1052,
#  'state': 'down'}
#
# {'__align': (),
#  'attrs': [('IFLA_IFNAME', 'dummy0'),
#            ('IFLA_TXQLEN', 1000),
#            ('IFLA_OPERSTATE', 'DOWN'),
#            ('IFLA_LINKMODE', 0),
#            ('IFLA_MTU', 1500),
#            ('UNKNOWN', {'header': {'length': 8, 'type': 50}}),
#            ('UNKNOWN', {'header': {'length': 8, 'type': 51}}),
#            ('IFLA_GROUP', 0),
#            ('IFLA_PROMISCUITY', 0),
#            ('IFLA_NUM_TX_QUEUES', 1),
#            ('IFLA_GSO_MAX_SEGS', 65535),
#            ('IFLA_GSO_MAX_SIZE', 65536),
#            ('IFLA_NUM_RX_QUEUES', 1),
#            ('IFLA_CARRIER', 1),
#            ('IFLA_QDISC', 'noop'),
#            ('IFLA_CARRIER_CHANGES', 0),
#            ('IFLA_PROTO_DOWN', 0),
#            ('IFLA_CARRIER_UP_COUNT', 0),
#            ('IFLA_CARRIER_DOWN_COUNT', 0),
#            ('IFLA_MAP', {'mem_start': 0, 'mem_end': 0, 'base_addr': 0, 'irq': 0, 'dma': 0, 'port': 0}),
#            ('IFLA_ADDRESS', '1a:9b:d9:99:96:69'),
#            ('IFLA_BROADCAST', 'ff:ff:ff:ff:ff:ff'),
#            ('IFLA_STATS64', {'rx_packets': 0, 'tx_packets': 0, 'rx_bytes': 0, 'tx_bytes': 0, 'rx_errors': 0, 'tx_errors': 0, 'rx_dropped': 0, 'tx_dropped': 0, 'multicast': 0, 'collisions': 0, 'rx_length_errors': 0, 'rx_over_errors': 0, 'rx_crc_errors': 0, 'rx_frame_errors': 0, 'rx_fifo_errors': 0, 'rx_missed_errors': 0, 'tx_aborted_errors': 0, 'tx_carrier_errors': 0, 'tx_fifo_errors': 0, 'tx_heartbeat_errors': 0, 'tx_window_errors': 0, 'rx_compressed': 0, 'tx_compressed': 0}),
#            ('IFLA_STATS', {'rx_packets': 0, 'tx_packets': 0, 'rx_bytes': 0, 'tx_bytes': 0, 'rx_errors': 0, 'tx_errors': 0, 'rx_dropped': 0, 'tx_dropped': 0, 'multicast': 0, 'collisions': 0, 'rx_length_errors': 0, 'rx_over_errors': 0, 'rx_crc_errors': 0, 'rx_frame_errors': 0, 'rx_fifo_errors': 0, 'rx_missed_errors': 0, 'tx_aborted_errors': 0, 'tx_carrier_errors': 0, 'tx_fifo_errors': 0, 'tx_heartbeat_errors': 0, 'tx_window_errors': 0, 'rx_compressed': 0, 'tx_compressed': 0}),
#            ('IFLA_XDP', '05:00:02:00:00:00:00:00'),
#            ('IFLA_LINKINFO', {'attrs': [('IFLA_INFO_KIND', 'dummy')]}),
#            ('IFLA_AF_SPEC', {'attrs': []})],
#  'change': 4294967295,
#  'event': 'RTM_DELLINK',
#  'family': 0,
#  'flags': 130,
#  'header': {'error': None,
#             'flags': 0,
#             'length': 580,
#             'pid': 0,
#             'sequence_number': 0,
#             'stats': Stats(qsize=0, delta=0, delay=0),
#             'target': 'localhost',
#             'type': 17},
#  'ifi_type': 1,
#  'index': 1052,
#  'state': 'down'}
